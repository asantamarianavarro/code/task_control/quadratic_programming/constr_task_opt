#edit the following line to add the librarie's header files
FIND_PATH(constr_task_opt_INCLUDE_DIR constr_task_opt.h /usr/include/constr_task_opt /usr/local/include/constr_task_opt)

FIND_LIBRARY(constr_task_opt_LIBRARY
    NAMES constr_task_opt
    PATHS /usr/lib /usr/local/lib) 

IF (constr_task_opt_INCLUDE_DIR AND constr_task_opt_LIBRARY)
   SET(constr_task_opt_FOUND TRUE)
ENDIF (constr_task_opt_INCLUDE_DIR AND constr_task_opt_LIBRARY)

IF (constr_task_opt_FOUND)
   IF (NOT constr_task_opt_FIND_QUIETLY)
      MESSAGE(STATUS "Found constr_task_opt: ${constr_task_opt_LIBRARY}")
   ENDIF (NOT constr_task_opt_FIND_QUIETLY)
ELSE (constr_task_opt_FOUND)
   IF (constr_task_opt_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "Could not find constr_task_opt")
   ENDIF (constr_task_opt_FIND_REQUIRED)
ENDIF (constr_task_opt_FOUND)

