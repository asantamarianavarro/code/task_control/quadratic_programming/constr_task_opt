# C++ library for Constrained Task Optimal Control of Unmanned Aerial Manipulators (UAM)

This library provides the functionalities for trajectory generation using optimal control.


**Related publications:** 

[Trajectory Generation for Unmanned Aerial Manipulators through Quadratic Programming](http://www.angelsantamaria.eu/publications/ral17)

R. Rossi, A. Santamaria-Navarro, J. Andrade-Cetto and P. Rocco

IEEE Robotics and Automation Letters, vol. 2, no. 2, pp. 389-396, Apr. 2017 


### Software dependencies

- [Boost](http://www.boost.org/) (already installed in most Ubuntu LTS)
  
- [Eigen 3](eigen.tuxfamily.org)

- [QPoases](https://projects.coin-or.org/qpOASES)

### Installation

- Clone the repository: `git clone https://gitlab.com/asantamarianavarro/code/task_control/quadratic_programming/constr_task_opt.git`
- Install:	`cd constr_task_opt/build && cmake -D CMAKE_BUILD_TYPE=RELEASE .. && make -j $(nproc) && sudo make install`

### Example of usage

- Checkout [kinton_arm_constr_task_opt](https://gitlab.com/asantamarianavarro/code/task_control/quadratic_programming/kinton_arm_constr_task_opt) project to use this library within ROS framework.

### Support material and multimedia

Please, visit: [**asantamaria's web page**](http://www.angelsantamaria.eu)
