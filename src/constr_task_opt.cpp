#include "constr_task_opt.h"

/** Constructor */
CConstrTaskOpt::CConstrTaskOpt(const int& num_vars, const int& num_constr, const int& nWSR)
{
    this->params_init_ = false;
    this->tasks_init_ = false;
    this->constrs_init_ = false;
    this->tasks_.clear();
    this->constrs_.clear();

    this->qpoases_ptr_ = new CQpoases(num_vars, num_constr, nWSR);
}

/** Destructor */
CConstrTaskOpt::~CConstrTaskOpt()
{
    if (this->qpoases_ptr_ != NULL)
    {
        delete this->qpoases_ptr_;
        this->qpoases_ptr_ = NULL;
    }
}

/** Set parameters */
void CConstrTaskOpt::set_parameters(const CParams& params, const CBounds& bounds)
{
    this->params_ = params;
    set_bounds(bounds);
    this->params_init_ = true;
}

/** Set bounds */
void CConstrTaskOpt::set_bounds(const CBounds& bounds)
{
    this->bounds_ = bounds;
}

/** Set tasks */
void CConstrTaskOpt::set_tasks(const std::vector<CTask>& tasks)
{
    if (this->params_init_)
    {
        for (int ii = 0; ii < tasks.size(); ++ii)
        {
            if (!this->tasks_init_)
            {
                CTaskFull ftask(tasks[ii]);
                this->tasks_.push_back(ftask);
            }
            else
                this->tasks_[ii].fill(tasks[ii]); // Add things to class full

            derivative_matrix(this->params_.tau_der, this->params_.tstep, this->tasks_[ii].Jac, this->tasks_[ii].DJac, this->tasks_[ii].Jac_old, this->tasks_[ii].DJac_old);
        }
        this->tasks_init_ = true;
    }
    else
        std::cerr << "[CConstrTaskOpt]: set_tasks error. Initialize parameters first." << std::endl;
}

/** Set constraints */
void CConstrTaskOpt::set_constraints(const std::vector<CConstr>& constrs)
{
    if (this->params_init_)
    {
        for (int ii = 0; ii < constrs.size(); ++ii)
        {
            if (!this->constrs_init_)
            {
                CConstrFull fconstr(constrs[ii]);
                this->constrs_.push_back(fconstr);
            }
            else
                this->constrs_[ii].fill(constrs[ii]); // Add things to class full

            derivative_matrix(this->params_.tau_der, this->params_.tstep, this->constrs_[ii].Jac, this->constrs_[ii].DJac, this->constrs_[ii].Jac_old, this->constrs_[ii].DJac_old);
        }
        this->constrs_init_ = true;
    }
    else
        std::cerr << "[CConstrTaskOpt]: set_constraints error. Initialize parameters first." << std::endl;
}

/** Set joints */
void CConstrTaskOpt::set_joints(const Eigen::VectorXd& jpos, const Eigen::VectorXd& jvel, const Eigen::VectorXd& weights_joints)
{
    this->joints_.curr_pos = jpos;
    this->joints_.curr_vel = jvel;
    this->joints_.Weights_joints = weights_joints.asDiagonal(); 

    // Initialize other object sizes
    this->joints_.ref_acc = Eigen::VectorXd::Zero(jpos.size());
    this->joints_.ref_acc_dual = Eigen::VectorXd::Zero(jpos.size()+this->constrs_.size());
    this->joints_.ref_vel = Eigen::VectorXd::Zero(jpos.size());
    this->joints_.ref_pos = Eigen::VectorXd::Zero(jpos.size());
}

/** Matrix derivative */
void CConstrTaskOpt::derivative_matrix(const double& tau_filt, const double& Tsample, const Eigen::MatrixXd& Jm, Eigen::MatrixXd& DJm_dt, Eigen::MatrixXd& Jm_old, Eigen::MatrixXd& DJm_dt_old)
{
    if (Tsample+tau_filt != 0.0)
        DJm_dt = (tau_filt*DJm_dt_old + Jm-Jm_old)/(Tsample+tau_filt);
    else
        DJm_dt = DJm_dt_old;
    Jm_old = Jm;
    DJm_dt_old = DJm_dt;
}

/** Call QP solver */
void CConstrTaskOpt::call_solver(Eigen::VectorXd& csidd_solution, Eigen::VectorXd& csidd_dual_solution, Eigen::MatrixXd& Csid_ref, Eigen::MatrixXd& Csi_ref)
{
    // Compute Hessian (H) and gradient vector (g)
    compute_cost_function();
    // Compute constraint matrix A and bounds vectors lbA and ubA
    compute_constraints();
    // Compute variables bounds (lb,ub)
    compute_prblm_bounds();
    // Call solver
    this->qpoases_ptr_->hotstart(this->params_.nWSR);
    // Integrate to get velocity and position references
    compute_ctrl_ref();
    csidd_solution = this->joints_.ref_acc;
    csidd_dual_solution = this->joints_.ref_acc_dual;
    Csid_ref = this->joints_.ref_vel;
    Csi_ref = this->joints_.ref_pos;
    // DEBUG
    // this->qpoases_ptr_->print_vars();
}

/** Compute const function */
void CConstrTaskOpt::compute_cost_function(void)
{
    int tdim = 0;
    for (int ii = 0; ii < this->tasks_.size(); ++ii)
        tdim = tdim + this->tasks_[ii].dim;

    Eigen::MatrixXd AJ = Eigen::MatrixXd::Zero(tdim,this->params_.num_vars);
    Eigen::VectorXd bJ = Eigen::VectorXd::Zero(tdim);
    Eigen::MatrixXd Weights_all = Eigen::MatrixXd::Identity(tdim,tdim);

    int row = 0;
    for (int ii = 0; ii < this->tasks_.size(); ++ii)
    {
        this->tasks_[ii].error = -this->tasks_[ii].error;
        compute_Ab(this->tasks_[ii].error, this->tasks_[ii].Jac, this->tasks_[ii].DJac, this->joints_.curr_vel, this->params_.l1, this->params_.l2, this->tasks_[ii].is_pos_space, this->tasks_[ii].normalize_jac, this->tasks_[ii].A, this->tasks_[ii].b);

        AJ.block(row,0,this->tasks_[ii].dim,this->params_.num_vars) = this->tasks_[ii].A;
        bJ.segment(row,this->tasks_[ii].dim) = this->tasks_[ii].b;
        Weights_all.block(row,row,this->tasks_[ii].dim,this->tasks_[ii].dim) = this->tasks_[ii].Weights; 
        row = row + this->tasks_[ii].dim;
    }

    //AJ = AJ*this->joints_.Weights_joints;
    AJ = AJ;

    compute_Hg(AJ, bJ, Weights_all, this->qpoases_ptr_->H, this->qpoases_ptr_->g);

    Eigen::MatrixXd Acc_weights_matrix = 2.0*qpOASES::EPS*Eigen::MatrixXd::Identity(this->qpoases_ptr_->H.rows(),this->qpoases_ptr_->H.cols());
    //Acc_weights_matrix.block(0,0,4,4) = MatrixXd::Identity(4,4)*1e2;
    this->qpoases_ptr_->H = this->qpoases_ptr_->H + Acc_weights_matrix;
}

/** Compute A and b matrices */
void CConstrTaskOpt::compute_Ab(const Eigen::VectorXd& f, const Eigen::MatrixXd& Jac, const Eigen::MatrixXd& DJac_dt, const Eigen::VectorXd& csi_dot, const double& l1, const double& l2, bool is_pos, bool normalize_jac, Eigen::MatrixXd& A, Eigen::VectorXd& b)
{
    if (Jac.lpNorm<Eigen::Infinity>() == 0)
        normalize_jac = false;

    Eigen::MatrixXd A1 = Eigen::MatrixXd::Zero(Jac.rows(),Jac.cols());
    Eigen::VectorXd b1 = Eigen::VectorXd::Zero(f.size());
    if (is_pos) 
    {
        A1 = Jac;
        b1 = -l1*f;
    }

    Eigen::VectorXd f2 = Eigen::VectorXd::Zero(f.size());

    if (is_pos) 
        f2 = A1*csi_dot - b1;
    else
        f2 = f;

    A = Jac;
    b = -(DJac_dt*csi_dot) - (l2*f2);
    // FIX: missing term with l1. Theoretically it should be:
    //  b = -(DJac_dt*csi_dot) - (l2*f2) - l1*A1*csi_dot;

    double norm = 1.0;
    if (normalize_jac) 
    {
        Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> solveivals(Jac.transpose()*Jac);
        Eigen::VectorXd eivals = solveivals.eigenvalues();
        norm = 0.0;
        for (int ii = 0; ii < eivals.size(); ++ii)
        {
            double aeval = std::abs(eivals(ii));
            if (aeval>norm)
                norm = aeval; 
        }
    }

    if (norm > 0.0)
    {
      A = Jac/sqrt(norm);
      b = b/sqrt(norm);
    }
}

/** Compute Hessian matrix (H) and gradient vector (g) */
void CConstrTaskOpt::compute_Hg(const Eigen::MatrixXd& A, const Eigen::VectorXd& b, const Eigen::MatrixXd& W, Eigen::MatrixXd& H, Eigen::VectorXd& g)
{  
    //H = A.transpose()*W.transpose().array().sqrt().matrix()*W.array().sqrt().matrix()*A;
    Eigen::MatrixXd A_weighted = A;
    Eigen::MatrixXd b_weighted = b;

    for (int ii = 0; ii < A.rows(); ++ii)
    {
        A_weighted.row(ii) = sqrt(W(ii,ii))*A.row(ii);
        b_weighted(ii) = sqrt(W(ii,ii))*b(ii);
    }
    H = A_weighted.transpose()*A_weighted;
    g = -A_weighted.transpose()*b_weighted;

}

/** Compute constraints */
void CConstrTaskOpt::compute_constraints(void)
{
    int row = 0;
    for (int ii = 0; ii < this->constrs_.size(); ++ii)
    {
        compute_constraints_A(this->constrs_[ii].error, this->constrs_[ii].Jac, this->constrs_[ii].DJac, this->constrs_[ii].dim, this->params_.num_vars, this->constrs_[ii].bound_type, this->joints_.curr_vel, this->params_.l1, this->params_.l2, this->constrs_[ii].is_pos_space, this->constrs_[ii].lb, this->constrs_[ii].ub, this->constrs_[ii].A);

        // this->qpoases_ptr_->A.block(row,0,this->constrs_[ii].dim, this->params_.num_vars) = this->constrs_[ii].A*this->joints_.Weights_joints;
        this->qpoases_ptr_->A.block(row,0,this->constrs_[ii].dim, this->params_.num_vars) = this->constrs_[ii].A;
        this->qpoases_ptr_->lbA.segment(row,this->constrs_[ii].dim) = this->constrs_[ii].lb;
        this->qpoases_ptr_->ubA.segment(row,this->constrs_[ii].dim) = this->constrs_[ii].ub;

        row = row + this->constrs_[ii].dim;
    }
}

/** Compute matrix A of constraints */
void CConstrTaskOpt::compute_constraints_A(Eigen::VectorXd& error, Eigen::MatrixXd& Jac, Eigen::MatrixXd& DJac, const int& dim, const int& num_vars, int& boundtype_JC, Eigen::VectorXd& csi_dot, double& l1, double& l2, bool& is_pos, Eigen::VectorXd& lbA, Eigen::VectorXd& ubA, Eigen::MatrixXd& AJC)
{
    Eigen::VectorXd bJC = Eigen::VectorXd::Zero(dim);
    compute_Ab( error, Jac, DJac, csi_dot, l1, l2, is_pos, false, AJC, bJC);

    Eigen::VectorXd ones_v = Eigen::VectorXd::Ones(dim);

    switch(boundtype_JC)
    {
        case 0:
            lbA = bJC;
            ubA = qpOASES::INFTY*ones_v;
            break;
        case 1:
            lbA = -qpOASES::INFTY*ones_v;
            ubA = bJC;
            break;
        case 2:
            lbA = bJC;
            ubA = bJC;
            break;
        default:
            lbA = -qpOASES::INFTY*ones_v;
            ubA = qpOASES::INFTY*ones_v;
    }
 
}

/** Compute problem bounds */
void CConstrTaskOpt::compute_prblm_bounds(void)
{
    Eigen::VectorXd f_pmin(this->params_.num_vars);
    Eigen::VectorXd f_pmax(this->params_.num_vars);
    Eigen::VectorXd f_vmin(this->params_.num_vars);
    Eigen::VectorXd f_vmax(this->params_.num_vars);

    Eigen::MatrixXd Eye_NumVars = Eigen::MatrixXd::Identity(this->params_.num_vars,this->params_.num_vars);
    Eigen::MatrixXd Zero_NumVars = Eigen::MatrixXd::Zero(this->params_.num_vars,this->params_.num_vars);

    f_pmin = this->joints_.curr_pos - this->bounds_.lpos;
    f_pmax = this->joints_.curr_pos - this->bounds_.upos;
    f_vmin = this->joints_.curr_vel - this->bounds_.lvel;
    f_vmax = this->joints_.curr_vel - this->bounds_.uvel;

    Eigen::MatrixXd DummyM(this->params_.num_vars,this->params_.num_vars);
    Eigen::VectorXd b_pmin(this->params_.num_vars);
    Eigen::VectorXd b_pmax(this->params_.num_vars);
    Eigen::VectorXd b_vmin(this->params_.num_vars);
    Eigen::VectorXd b_vmax(this->params_.num_vars);
    Eigen::VectorXd b_amin(this->params_.num_vars);
    Eigen::VectorXd b_amax(this->params_.num_vars);

    // Position
    compute_Ab(f_pmin, Eye_NumVars, Zero_NumVars, this->joints_.curr_vel, this->params_.l1, this->params_.l2, true, false, DummyM, b_pmin);
    compute_Ab(f_pmax, Eye_NumVars, Zero_NumVars, this->joints_.curr_vel, this->params_.l1, this->params_.l2, true, false, DummyM, b_pmax);

    // Velocity
    compute_Ab(f_vmin, Eye_NumVars, Zero_NumVars, this->joints_.curr_vel, this->params_.l1, this->params_.l2, false, false, DummyM, b_vmin);
    compute_Ab(f_vmax, Eye_NumVars, Zero_NumVars, this->joints_.curr_vel, this->params_.l1, this->params_.l2, false, false, DummyM, b_vmax);

    // Acceleration
    for ( int ii=0; ii<this->params_.num_vars; ii++ )
    {
        b_amin(ii) = this->bounds_.lacc(ii);
        b_amax(ii) = this->bounds_.uacc(ii);        
    }

    // Overall Limits
    for ( int ii=0; ii<this->params_.num_vars; ii++ )
    {
        // this->qpoases_ptr_->lb(ii) = (1.0/this->joints_.Weights_joints(ii,ii))*std::max(std::max(b_pmin(ii), b_vmin(ii)), b_amin(ii));
        // this->qpoases_ptr_->ub(ii) = (1.0/this->joints_.Weights_joints(ii,ii))*std::min(std::min(b_pmax(ii), b_vmax(ii)), b_amax(ii));
        this->qpoases_ptr_->lb(ii) = std::max(std::max(b_pmin(ii), b_vmin(ii)), b_amin(ii));
        this->qpoases_ptr_->ub(ii) = std::min(std::min(b_pmax(ii), b_vmax(ii)), b_amax(ii));

        if (this->qpoases_ptr_->ub(ii) < this->qpoases_ptr_->lb(ii))
        {
            this->qpoases_ptr_->ub(ii) = this->qpoases_ptr_->lb(ii);
            std::cout << "****************************************************" << std::endl;
            std::cout << "Constr_task at: "<< __LINE__ << " Constraints not consistent! for Joint: " << ii << std::endl;
            std::cout << "****************************************************" << std::endl;
        }
    }
}

/** Compute resulting control reference */
void CConstrTaskOpt::compute_ctrl_ref(void)
{
    //this->joints_.ref_acc = this->joints_.Weights_joints*this->qpoases_ptr_->xOpt;
    this->joints_.ref_acc = this->qpoases_ptr_->xOpt;
    this->joints_.ref_acc_dual = this->qpoases_ptr_->yOpt;
    this->joints_.ref_vel = this->joints_.curr_vel + this->joints_.ref_acc*this->params_.tstep_horizon;
    this->joints_.ref_pos = this->joints_.curr_pos + this->joints_.curr_vel*this->params_.tstep_horizon + 0.5*this->joints_.ref_acc*std::pow(this->params_.tstep_horizon,2);
}
