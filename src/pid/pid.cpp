#include <pid/pid.h>
#include <cmath>
#include <iostream>

PID::PID(void)
{
}

PID::~PID(void)
{
}

void PID::reset(void)
{
  // set prev and integrated errors to zero
  this->prev_error_ = 0.0;
  this->int_error_ = 0.0;
}

void PID::update(double curr_error, double dt)
{
  double diff;
  double p_term;
  double i_term;
  double d_term;

  // integration with windup guarding
  this->int_error_ += (curr_error * dt);
  if (this->int_error_ < -(this->windup_guard_))
      this->int_error_ = -(this->windup_guard_);
  else if (this->int_error_ > this->windup_guard_)
      this->int_error_ = this->windup_guard_;

  // differentiation
  if (dt == 0.0 || std::isnan(curr_error) || std::isinf(curr_error))
  {
    diff = 0.0;
  }
  else
  {
    diff = (curr_error - this->prev_error_) / dt;
  }

  // scaling
  p_term = this->kp_ * curr_error;
  i_term = this->ki_ * this->int_error_;
  d_term = this->kd_ * diff;

  // std::cout << "windup_guard_" << std::endl;
  // std::cout << windup_guard_ << std::endl;
  // std::cout << "dt" << std::endl;
  // std::cout << dt << std::endl;
  // std::cout << "curr_error" << std::endl;
  // std::cout << curr_error << std::endl;
  // std::cout << "int_error_" << std::endl;
  // std::cout << int_error_ << std::endl;
  // std::cout << "i_term" << std::endl;
  // std::cout << i_term << std::endl;

  // summation of terms
  this->control_ = p_term + i_term + d_term;

  // save current error as previous error for next iteration
  this->prev_error_ = curr_error;   
}